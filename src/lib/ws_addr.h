/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _WS_ADDR_H_
#define _WS_ADDR_H_

#include <arpa/inet.h> // uses in_addr
#include <stdbool.h>

/** The address structure
  * 
  * in_addr_t doc can be found in `man inet`.
  * The port is automatically set to DEFAULT_PORT.
  *
  */
typedef struct{
  struct in_addr* address; //!< The IPaddress
  int port;                //!< The meta server port
}ws_addr_t;

ws_addr_t* ws_addr_create();
ws_addr_t* ws_addr_copy(const ws_addr_t*);
void       ws_addr_free(ws_addr_t**);
char*      ws_addr_to_str(ws_addr_t*);
int        ws_addr_from_str(ws_addr_t**, const char*);
bool       ws_addr_eq(ws_addr_t*, ws_addr_t*);
#endif // _WS_ADDR_H_
