/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \file lenstring.h
  *
  * The header file of string_t implementation. A string with length
  * member support.
  *
  */

#ifndef _LEN_STRING_H_
#define _LEN_STRING_H_

#include <stdlib.h>   // Uses size_t
#include <stdbool.h>

/** A simple string with length member support
  *
  */
typedef struct  {
  char *ptr;   //!< The native c-string
  size_t len;  //!< The length
}string_t;

string_t* lenstring_create();
void    lenstring_free(string_t**);

void lenstring_set(string_t*, const char*);
string_t* lenstring_create_str();

bool lenstring_contains(string_t*, char*);

#endif // !_LEN_STRING_H_
