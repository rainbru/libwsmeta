/*
 * Copyright 2016-2018 Jérôme Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ws_list.h"

#include <stdlib.h> // Uses malloc
#include <stdio.h>

/** Allocate a new empty address list
 *
 */
ws_list_t*
ws_list_create()
{
  ws_list_t* ret = malloc(sizeof(ws_list_t));
  ret->start = NULL;
  return ret;
}

/** Free the memory used by the given list
  *
  * \param node The list to be freed
  *
  */
void
ws_list_free(ws_list_t** li)
{
  free(*li);
  *li = NULL;
}

/** Allocates a new list node
  *
  */
ws_list_node_t*
ws_list_node_create()
{
  ws_list_node_t* ret = malloc(sizeof(ws_list_node_t));
  ret->data = NULL;
  ret->next = NULL;
  return ret;

}

/** Free the memory used by a list node
  *
  * \param node The node to be freed
  *
  */
void
ws_list_node_free(ws_list_node_t** node)
{
  free(*node);
  *node = NULL;
}


/** Returns the length of a list in number of nodes
  *
  * \param list The list
  *
  */
size_t
ws_list_length(ws_list_t* list)
{
  size_t ret = 0;
  ws_list_node_t* last = list->start;
  while(last)
    {
      last = last->next;
      ++ret;
    }
  return ret;
}

/** Add a node to the end of the given list
  *
  * \param li The list to add the node to
  * \param ln The node to be added to the list
  *
  */
void
ws_list_add(ws_list_t* li, ws_list_node_t* ln)
{
  if (!li->start)
    li->start = ln;
  else
    {
      // Get the last node
      ws_list_node_t* last = li->start;
      while(last->next)
	{
	  last = last->next;
	}
      // Add to the last element
      last->next = ln;
    }
}

/** Add the givzen node only if its data is not yet in the list
  *
  * If the node's data is NULL, the node isn't added.
  *
  * \param li The list to add the node to
  * \param ln The node to be added to the list
  *
  */
void
ws_list_add_unique(ws_list_t* li, ws_list_node_t* ln)
{

  if (!ws_list_exists(li, ln->data))
    ws_list_add(li, ln);
    
}

/** Search for the given address and returns its index
  *
  * \param li The list
  * \param a  The address to be searched
  *
  * \return The index of the list or -1 if not found.
  *
  */
int
ws_list_find(ws_list_t* li, ws_addr_t* a)
{
  if (!li->start || a == NULL)
    return -1;
  
  int idx = 0;
  ws_list_node_t* now = li->start;
  while (now->data)
    {
      if( ws_addr_eq(now->data, a))
	return idx;
      
      ++idx;
      now = now->next;
    }
  return -1;
}

/** Return true if an address already exists in the given list
  *
  * \param li The list
  * \param a  The address to be searched for
  *
  */
bool
ws_list_exists(ws_list_t* li, ws_addr_t* a)
{
  if (ws_list_find(li, a) == -1)
    {
      return false;
    }
  else
    {
      return true;
    }
}
