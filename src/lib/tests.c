/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <stdlib.h> // Uses EXIT_FAILURE
#include <stdio.h>  // Uses printf()

#include "wsmeta_t.h"
#include "ws_addr_t.h"
#include "lenstring_t.h"
#include "ws_list_t.h"

 int
 main(void)
 {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = wsmeta_suite();
    sr = srunner_create(s);

    srunner_set_fork_status(sr, CK_NOFORK);
    srunner_add_suite(sr, wsaddr_suite());
    srunner_add_suite(sr, lenstring_suite());
    srunner_add_suite(sr, wslist_suite());
    srunner_set_log (sr, "test.log");
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    printf("Number failed : %d\n", number_failed);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
 }
