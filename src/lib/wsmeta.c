/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "wsmeta.h"

#include <stdlib.h>
#include <string.h>   // Uses memcpy
#include <curl/curl.h>

#include "lenstring.h"


/** Create an empty wsmeta_t pointer
  *
  * You will have to free it calling wsmeta_free().
  *
  */
wsmeta_t* wsmeta_create()
{
  wsmeta_t* ret = malloc(sizeof(wsmeta_t));
  ret->uri = NULL;
  return ret;
}

/** Free the memory used by a wsmeta_t object
  *
  *
  */
void
wsmeta_free(wsmeta_t** meta)
{
  if ((*meta)->uri)
    free((*meta)->uri);
  free(*meta);
  *meta = NULL;
}

/** Copy the content of ptr to s
 *
 */
size_t writefunc(void *ptr, size_t size, size_t nmemb, string_t *s)
{
  // All pointers should be set
  if (ptr == NULL || s == NULL)
    exit(EXIT_FAILURE);
  
  size_t new_len = s->len + size*nmemb;
  s->ptr = realloc(s->ptr, new_len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc() failed\n");
    exit(EXIT_FAILURE);
  }
  memcpy(s->ptr + s->len, ptr, size * nmemb);
  s->ptr[new_len] = '\0';
  s->len = new_len;

  return size*nmemb;
}

/** Download the given entry URL
  *
  * \param m        A correctly initialised wsmeta_t object.
  * \param uri      The entry file to be downloaded.
  * \param interval Update interval.
  *
  * \return true if successfull, false otherwise
  *
  */
bool
wsmeta_entry(wsmeta_t* m, const char* uri, int interval)
{
  CURL *curl;
  CURLcode res;

  // Copy the URI to m
  m->uri = malloc(sizeof(char) * (strlen(uri) + 1));
  strncpy(m->uri, uri, strlen(uri) + 1);
  
  // Get the URI content into a string
  curl_global_init(CURL_GLOBAL_DEFAULT);
  curl = curl_easy_init();
  
  if(curl) {
    string_t* s = lenstring_create();

    curl_easy_setopt(curl, CURLOPT_URL, uri);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
    res = curl_easy_perform(curl);


    /* always cleanup */
    curl_easy_cleanup(curl);

    if (res != CURLE_OK){
      printf("Error: curl returned %d\n", res);
    }
    else {
      int i = 0;
      if (s)
	printf("SUCESS: %d: %s\n", ++i, s->ptr);
      else
	printf("SUCESS: %d: (But invalid lenstring)\n", ++i, s->ptr);
    }
    lenstring_free(&s);
    
    return res == CURLE_OK;
  }
}
