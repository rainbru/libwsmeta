/*
 * Copyright 2016-2018 Jérôme Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <arpa/inet.h> // uses inet_aton
#include <stdlib.h> // uses malloc

#include "ws_addr.h"

#include "wsmeta_config.h"    // Usess DEFAULT_PORT

START_TEST(test_wsaddr_create)
{
  ws_addr_t* mt = ws_addr_create();
  ck_assert_ptr_ne(mt, NULL);
  ws_addr_free(&mt);
}
END_TEST

/* From a newly created address, the returned string should be empty
 */
START_TEST(test_wsaddr_tostr_null)
{
  ws_addr_t* mt = ws_addr_create();
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "");
  ws_addr_free(&mt);
}
END_TEST

/** Test that the port of a newly created adress default to DEFAUTL_PORT */
START_TEST(test_wsaddr_default_port)
{
  ws_addr_t* mt = ws_addr_create();
  ck_assert_int_eq(mt->port, DEFAULT_PORT);
  ws_addr_free(&mt);
}
END_TEST

START_TEST(test_wsaddr_tostr_ip)
{
  ws_addr_t* mt = ws_addr_create();
  mt->address = malloc(sizeof(struct in_addr));
  inet_aton("127.0.0.1", mt->address);
    
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "127.0.0.1:16001");
  ws_addr_free(&mt);
}
END_TEST


START_TEST(test_wsaddr_tostr_ip_port)
{
  ws_addr_t* mt = ws_addr_create();
  mt->address = malloc(sizeof(struct in_addr));
  inet_aton("127.0.0.1", mt->address);
  mt->port = 1000;
  
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "127.0.0.1:1000");
  ws_addr_free(&mt);
}
END_TEST


START_TEST(test_wsaddr_fromstr_ip_port)
{
  ws_addr_t* mt = ws_addr_create();
  int ret = ws_addr_from_str(&mt, "192.168.12.14:1203");
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "192.168.12.14:1203");
  ck_assert_int_eq(ret, 2);
  ws_addr_free(&mt);
}
END_TEST

START_TEST(test_wsaddr_fromstr_ip)
{
  ws_addr_t* mt = ws_addr_create();
  int ret = ws_addr_from_str(&mt, "192.168.12.14");
  
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "192.168.12.14:16001");
  ck_assert_int_eq(ret, 1);  // Only one extracted value
  ws_addr_free(&mt);
}
END_TEST

START_TEST(test_wsaddr_fromstr_err)
{
  ws_addr_t* mt = ws_addr_create();
  int ret = ws_addr_from_str(&mt, "");
  
  char* c = ws_addr_to_str(mt);
  ck_assert_str_eq(c, "");
  ck_assert_int_eq(ret, 0);
  ws_addr_free(&mt);
}
END_TEST

START_TEST(test_wsaddr_eq1)
{
  ws_addr_t* a1 = ws_addr_create();
  ws_addr_from_str(&a1, "127.0.0.1");

  ws_addr_t* a2 = ws_addr_create();
  ws_addr_from_str(&a2, "127.0.0.1");

  ck_assert(ws_addr_eq(a1, a2));
  ws_addr_free(&a1);
  ws_addr_free(&a2);
}
END_TEST

START_TEST(test_wsaddr_eq2)
{
  ws_addr_t* a1 = ws_addr_create();
  ws_addr_from_str(&a1, "127.0.0.1");

  ws_addr_t* a2 = ws_addr_create();
  ws_addr_from_str(&a2, "192.168.0.1");

  ck_assert(!ws_addr_eq(a1, a2));
  ws_addr_free(&a1);
  ws_addr_free(&a2);
}
END_TEST

START_TEST(test_wsaddr_eq_port)
{
  ws_addr_t* a1 = ws_addr_create();
  ws_addr_from_str(&a1, "127.0.0.1:8001");

  ws_addr_t* a2 = ws_addr_create();
  ws_addr_from_str(&a2, "127.0.0.1:9001");

  ck_assert(!ws_addr_eq(a1, a2));
  ws_addr_free(&a1);
  ws_addr_free(&a2);
}
END_TEST

START_TEST(test_wsaddr_copy)
{
  ws_addr_t* a1 = ws_addr_create();
  ws_addr_from_str(&a1, "127.0.0.1:8001");

  ws_addr_t* a2 = ws_addr_copy(a1);

  ck_assert(ws_addr_eq(a1, a2));
  ws_addr_free(&a1);
  ws_addr_free(&a2);
}
END_TEST

/** Test that copying a NULL addr doesn't cause a segfault */
START_TEST(test_wsaddr_copy_nulladdr)
{
  ws_addr_t* a1 = ws_addr_create();
  a1->port = 1212;
  
  ws_addr_t* a2 = ws_addr_copy(a1);
  ck_assert_int_eq(a1->port, a2->port);
  ck_assert(a2->address == NULL);
  
  ws_addr_free(&a1);
  ws_addr_free(&a2);
}
END_TEST


Suite * wsaddr_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("WSaddr");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_wsaddr_create);
    tcase_add_test(tc_core, test_wsaddr_default_port);
    tcase_add_test(tc_core, test_wsaddr_tostr_null);
    tcase_add_test(tc_core, test_wsaddr_tostr_ip);
    tcase_add_test(tc_core, test_wsaddr_tostr_ip_port);
    tcase_add_test(tc_core, test_wsaddr_fromstr_ip_port);
    tcase_add_test(tc_core, test_wsaddr_fromstr_ip);
    tcase_add_test(tc_core, test_wsaddr_fromstr_err);
    tcase_add_test(tc_core, test_wsaddr_eq1);
    tcase_add_test(tc_core, test_wsaddr_eq2);
    tcase_add_test(tc_core, test_wsaddr_eq_port);
    tcase_add_test(tc_core, test_wsaddr_copy);
    tcase_add_test(tc_core, test_wsaddr_copy_nulladdr);
    
    suite_add_tcase(s, tc_core);

    return s;
}
