/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ws_addr.h"

#include <stdlib.h> // Uses malloc
#include <stdio.h> // Uses malloc
#include <string.h> // Uses itoa

#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

#include "wsmeta_config.h"    // Uses DEFAULT_PORT

static char* addr_str = NULL;

/** Free the static string if needed 
  *
  */
void
free_addr_str()
{
  if (addr_str)
    free(addr_str);
  
  addr_str = NULL;
}

/** Create an empty address
  *
  */
ws_addr_t*
ws_addr_create()
{
  ws_addr_t* ret = malloc(sizeof(ws_addr_t));
  ret->address = NULL;
  ret->port = DEFAULT_PORT;
  return ret;
}

/** A simple copy constructor
  *
  */
ws_addr_t*
ws_addr_copy(const ws_addr_t* orig)
{
  ws_addr_t* ret = ws_addr_create();

  if (orig->address)
    {
      ret->address = malloc(INET_ADDRSTRLEN);
      memcpy(ret->address, orig->address, INET_ADDRSTRLEN);
    }
  
  //ret->address = orig->address;
  ret->port = orig->port;
}


void
ws_addr_free(ws_addr_t** wa)
{
  free(*wa);
  free_addr_str();
}

/** Return the adress part of the struct to string
  *
  * Do not add the optional port part.
  *
  */
char*
ws_addr_to_str(ws_addr_t* addr)
{
  
  if (addr->address == NULL)
    {
      free_addr_str();
      return "";
    }
  else
    {
      if (addr->port == 0)
	{
	  addr_str = malloc(INET_ADDRSTRLEN);
	  //	  return inet_ntoa(*addr->address);
	  inet_ntop(AF_INET, addr->address, addr_str, INET_ADDRSTRLEN);
	  return addr_str;
	}
      else
	{
	  // Return a 127.0.0.1:1000 string
	  char full[80];
	  addr_str = malloc(INET_ADDRSTRLEN);
	  inet_ntop(AF_INET, addr->address, addr_str, INET_ADDRSTRLEN);
	  strcpy(full, addr_str);
	  strcat(full, ":");

	  char prt[20];
	  snprintf(prt, sizeof(prt), "%d", addr->port);
	  strcat(full, prt);

	  free_addr_str();
	  addr_str = (char*)malloc(sizeof(char) * strlen(full) + 1);
	  strcpy(addr_str, full);
	  return addr_str;
	}
    }
}

/** Extract a ws_addr_t from an adress:port string
 *
  * Return 0 if an error ocurred 
  * 1 If only address has been extracted
  * 2 if both addres and port have been extracted 
  *
  */
int
ws_addr_from_str(ws_addr_t** addr, const char* str)
{
  char *ip = NULL;
  char* port = NULL;

  // First, use strsep to feed ip and port
  if (strlen(str) == 0)
    return 0;
  
  char *token, *string, *tofree;
  tofree = string = strdup(str);
  
  while ((token = strsep(&string, ":")) != NULL)
    {
      if (ip == NULL)
	ip = strdup(token);
      else
	if (port == NULL)
	  port = strdup(token);
    }
  
  free(tofree);

  // Then, use getaddrinfo to convert
  struct addrinfo hints,*res;
  int n;
  int err;
  
  memset(&hints,0,sizeof(hints));
  hints.ai_family = AF_UNSPEC; 
  hints.ai_socktype = SOCK_DGRAM;
  err = getaddrinfo(ip, port, &hints,&res);
  if(err != 0){
    perror("getaddrinfo");
    printf("getaddrinfo %s\n",strerror(errno));
    printf("getaddrinfo : %s \n",gai_strerror(err));
    return 0;
  }

  // Finally, convert sockaddr_in in_addr
  struct sockaddr_in *sarrr_in;
  sarrr_in = (struct sockaddr_in *)res->ai_addr; 
  in_addr_t ip_address = ((struct sockaddr_in*)sarrr_in)->sin_addr.s_addr;
  (*addr)->address = malloc(sizeof(struct in_addr));
  (*addr)->address->s_addr = ip_address;
  freeaddrinfo(res);

  // Handle port : do not take the port from getaddrinfo (in network byte order)
  // And finally handle return value
  if (port != NULL)
    {
      (*addr)->port = atoi(port);
      return 2;
    }
  else
    return 1;
  
  
  //  return 0;
}

/** Check for ws_addr equality
 *
 */
bool
ws_addr_eq(ws_addr_t* a1, ws_addr_t* a2)
{
  return (strcmp(ws_addr_to_str(a1),
		 ws_addr_to_str(a2)) == 0);
}

