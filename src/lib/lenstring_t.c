/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <check.h>

#include "lenstring.h"

START_TEST(test_ctor_empty)
{
  string_t* s = lenstring_create();
  ck_assert_str_eq(s->ptr, "");
  ck_assert_int_eq(s->len, 0);
  lenstring_free(&s);
}
END_TEST

START_TEST(test_lenstring_set)
{
  string_t* s = lenstring_create();
  lenstring_set(s, "aze");
  ck_assert_str_eq(s->ptr, "aze");
  ck_assert_int_eq(s->len, 3);
  lenstring_free(&s);
}
END_TEST

START_TEST(test_lenstring_create_str)
{
  string_t* s = lenstring_create_str("aze");
  ck_assert_str_eq(s->ptr, "aze");
  ck_assert_int_eq(s->len, 3);
  lenstring_free(&s);
}
END_TEST

START_TEST(test_contains_1)
{
  string_t* s = lenstring_create();
  lenstring_set(s, "azezer");
  
  ck_assert(lenstring_contains(s, "ze"));
  ck_assert(lenstring_contains(s, "zz") == false);
  lenstring_free(&s);
}
END_TEST



Suite * lenstring_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("lenstring");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_ctor_empty);
    tcase_add_test(tc_core, test_contains_1);
    tcase_add_test(tc_core, test_lenstring_set);
    tcase_add_test(tc_core, test_lenstring_create_str);
    
    suite_add_tcase(s, tc_core);

    return s;
}
