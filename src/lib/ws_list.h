/*
 * Copyright 2016-2018 Jérôme Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _WS_LIST_H_
#define _WS_LIST_H_

#include "ws_addr.h"

/** A list node struct */
typedef struct ws_list_node{
  ws_addr_t*      data;               //!< The address data
  struct ws_list_node* next;          //!< A pointer to the next node
}ws_list_node_t;


/** A List type */
typedef struct{
  ws_list_node_t* start;              //!< A pointer to the first node
} ws_list_t;


// ws_list-related functions
ws_list_t* ws_list_create();
void       ws_list_free(ws_list_t**);
size_t     ws_list_length(ws_list_t*);
void       ws_list_add(ws_list_t*, ws_list_node_t*);
void       ws_list_add_unique(ws_list_t*, ws_list_node_t*);
int        ws_list_find(ws_list_t*, ws_addr_t*);
bool       ws_list_exists(ws_list_t*, ws_addr_t*);

// ws_list_node-related functions
ws_list_node_t* ws_list_node_create();
void            ws_list_node_free(ws_list_node_t**);

#endif // !_WS_LIST_H_
