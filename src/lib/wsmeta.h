/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \mainpage libwsmeta documentation
  *
  * \section intro_sec Introduction
  *
  * libwsmeta is a ansi C websocket-based metaserver library
  *  designed to be used with cmake as git submodule.
  *
  * From the client point of view, the starting point could be wsmeta_t.
  * Internally, the addresss is stored in a ws_addr_t structure.
  *
  */

#ifndef _WSMETA_H_
#define _WSMETA_H_

#include <stdbool.h> // Uses bool

/** The main meta structure
  *
  * You may want to create it with a call to wsmeta_entry() and must free
  * it with wsmeta_free().
  *
  */
typedef struct {
  char* uri; //!<  The entry point URI
  
} wsmeta_t;

wsmeta_t* wsmeta_create();
void    wsmeta_free(wsmeta_t**);

bool wsmeta_entry(wsmeta_t*, const char* uri, int interval);


#endif // !_WSMETA_H_
