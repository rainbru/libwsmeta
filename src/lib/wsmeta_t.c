/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <check.h>

#include "wsmeta.h"
#include "wsmeta.c" // Be able to test internal functions (writefunc...)

START_TEST(test_wsmeta_create)
{
  wsmeta_t* mt = wsmeta_create();
  ck_assert_ptr_ne(mt, NULL);
  wsmeta_free(&mt);
}
END_TEST

START_TEST(test_wsmeta_free_uri)
{
  wsmeta_t* mt = wsmeta_create();
  // Should free malloc'ed uri string
  mt->uri = strdup("test_string");
  wsmeta_free(&mt);
  ck_assert_ptr_eq(mt, NULL);
}
END_TEST

/* Test exit value when ptr pinter is null
 *
 */
START_TEST(test_writefunc_null_void_ptr)
{
  string_t* s = lenstring_create();
  lenstring_set(s, "aze");
  writefunc(NULL, 0, 0, s);
}
END_TEST

START_TEST(test_writefunc_null_str_ptr)
{
  string_t* s = lenstring_create();
  lenstring_set(s, "aze");
  writefunc(s, 0, 0, NULL);
}
END_TEST

START_TEST(test_writefunc_real)
{
  string_t* s = lenstring_create();
  lenstring_set(s, "aze");
  string_t* s2 = lenstring_create();
  size_t i = writefunc(s->ptr, 1, 3, s2);
  ck_assert_str_eq(s2->ptr, "aze");
  ck_assert_int_eq(i, 3);
  lenstring_free(&s);
  lenstring_free(&s2);
}
END_TEST

START_TEST(test_hostname_exists)
{
  wsmeta_t* mt = wsmeta_create();
  const char* uri = "https://rainbru.bitbucket.io/rainbrurph.entry";
  bool ret = wsmeta_entry(mt, uri, 600);
  ck_assert_int_eq(ret, true);
  wsmeta_free(&mt);
}
END_TEST

START_TEST(test_hostname_doesnt_exist)
{
  wsmeta_t* mt = wsmeta_create();
  bool ret = wsmeta_entry(mt, "rainbrurph-doesntexist.org", 600);
  ck_assert_int_eq(ret, false);
  wsmeta_free(&mt);
}
END_TEST

Suite * wsmeta_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("WSmeta");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_wsmeta_create);
    tcase_add_test(tc_core, test_wsmeta_free_uri);
    tcase_add_exit_test(tc_core, test_writefunc_null_void_ptr, 1);
    tcase_add_exit_test(tc_core, test_writefunc_null_str_ptr, 1);
    tcase_add_test(tc_core, test_writefunc_real);
    tcase_add_test(tc_core, test_hostname_exists);
    tcase_add_test(tc_core, test_hostname_doesnt_exist);

    suite_add_tcase(s, tc_core);

    return s;
}

