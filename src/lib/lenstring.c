/*
 * Copyright 2016-2018 Jerome Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lenstring.h"

#include <stdio.h>  // Uses fprintf()
#include <string.h> // Uses strstr()

/** Initialise an empty string structure
  *
  * \param s the structure to be initialised.
  * 
  * \param A newly malloc'ed (must be free'd later) string_t object.
  *
  */
string_t*
lenstring_create() {
  string_t* s = malloc(sizeof(string_t));
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
  return s;
}

/** Free the given lenstring
  *
  * \param s the string to be freed.
  *
  */
void
lenstring_free(string_t** s)
{
  if ((*s)->ptr)
    free((*s)->ptr);
  
  free(*s);
}


/** Test if a lenstring contains a C string
  *
  * \param haystack The string to be tested.
  * \param needle   The string to be tested.
  *
  * \return true if needle is contained in the haystack.
  *
  */
bool
lenstring_contains(string_t* haystack, char* needle)
{
  return strstr(haystack->ptr, needle) != NULL;
}

/** Set the content of thelenstring from an existing C string 
  *
  */
void
lenstring_set(string_t* s , const char* val)
{
  int size = strlen(val);

  s->len = size;
  s->ptr = (char*) realloc(s->ptr, (size + 1) * sizeof(char));
  strcpy(s->ptr, val);
}

string_t*
lenstring_create_str(const char* val)
{
  string_t* ret = lenstring_create();
  lenstring_set(ret, val);
  return ret;
}
