/*
 * Copyright 2016-2018 Jérôme Pasquier
 *
 * This file is part of libwsmeta.
 *
 * libwsmeta is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwsmeta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwsmeta.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <check.h>

#include "ws_list.h"

#include "wsmeta_config.h"    // Usess DEFAULT_PORT

START_TEST(test_wslist_create)
{
  ws_list_t* li = ws_list_create();
  ck_assert_ptr_ne(li, NULL);
  ws_list_free(&li);
}
END_TEST

START_TEST(test_wslist_free)
{
  ws_list_t* li = ws_list_create();
  ws_list_free(&li);
  ck_assert_ptr_eq(li, NULL);
}
END_TEST

START_TEST(test_wslist_node_create)
{
  ws_list_node_t* ln = ws_list_node_create();
  ck_assert_ptr_ne(ln, NULL);
  ws_list_node_free(&ln);
}
END_TEST

START_TEST(test_wslist_node_create_values)
{
  ws_list_node_t* ln = ws_list_node_create();
  ck_assert_ptr_eq(ln->data, NULL);
  ck_assert_ptr_eq(ln->next, NULL);
 
  ws_list_node_free(&ln);
}
END_TEST

START_TEST(test_wslist_node_free)
{
  ws_list_node_t* ln = ws_list_node_create();
  ws_list_node_free(&ln);
  ck_assert_ptr_eq(ln, NULL);
}
END_TEST

START_TEST(test_wslist_length_0)
{
  ws_list_t* li = ws_list_create();
  ck_assert_int_eq(ws_list_length(li), 0);
  ws_list_free(&li);
}
END_TEST

START_TEST(test_wslist_add)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_list_add(li, ln);
  ck_assert_int_eq(ws_list_length(li), 1);
  ws_list_free(&li);
  ws_list_node_free(&ln);
}
END_TEST

START_TEST(test_wslist_length_2)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln1 = ws_list_node_create();
  ws_list_add(li, ln1);
  ws_list_node_t* ln2 = ws_list_node_create();
  ws_list_add(li, ln2);
  ck_assert_int_eq(ws_list_length(li), 2);
  ws_list_free(&li);
}
END_TEST

START_TEST(test_wslist_add_unique)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_list_add_unique(li, ln);
  ck_assert_int_eq(ws_list_length(li), 1);
  ws_list_free(&li);
  ws_list_node_free(&ln);
}
END_TEST

/// Test if adding a node with a valid data pointer works
START_TEST(test_wslist_add_unique_addr)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add_unique(li, ln);
  ck_assert_int_eq(ws_list_length(li), 1);
  ws_list_free(&li);
  ws_addr_free(&mt);
  ws_list_node_free(&ln);
}
END_TEST

/// Add a second, non-unique node should fail
START_TEST(test_wslist_add_unique_addr2)
{
  ws_list_t* li = ws_list_create();
  
  // First item, does not exist, should add
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add_unique(li, ln);
  ck_assert_int_eq(ws_list_length(li), 1);

  // Second item already in list, shouldn't add
  ws_list_node_t* ln2 = ws_list_node_create();
  ws_addr_t* mt2 = ws_addr_create();
  ws_addr_from_str(&mt2, "192.168.12.14:1203");
  ln2->data = mt2;
  ws_list_add_unique(li, ln2);
  ck_assert_int_eq(ws_list_length(li), 1);

  // Free
  ws_list_free(&li);
  ws_addr_free(&mt);
  ws_list_node_free(&ln);
  ws_addr_free(&mt2);
  ws_list_node_free(&ln2);
}
END_TEST

/// Add a second, unique node should work
START_TEST(test_wslist_add_unique_addr3)
{
  ws_list_t* li = ws_list_create();
  //
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add_unique(li, ln);
  //
  ws_list_node_t* ln2 = ws_list_node_create();
  ws_addr_t* mt2 = ws_addr_create();
  ws_addr_from_str(&mt2, "192.168.12.15:1213");
  ln2->data = mt2;
  ws_list_add_unique(li, ln2);
  //
  ck_assert_int_eq(ws_list_length(li), 2);
  ws_list_free(&li);
  ws_addr_free(&mt);
  ws_list_node_free(&ln);
  ws_addr_free(&mt2);
  ws_list_node_free(&ln2);
}
END_TEST

/// Test if adding a node with a valid data pointer works
START_TEST(test_wslist_find1)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add_unique(li, ln);
  ck_assert_int_eq(ws_list_find(li, mt), 0);
  ws_list_free(&li);
  ws_addr_free(&mt);
  ws_list_node_free(&ln);
}
END_TEST

/// Searching for a non-existing address should fail
START_TEST(test_wslist_find2)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add_unique(li, ln);
  ws_addr_t* mt2 = ws_addr_create();
  ws_addr_from_str(&mt2, "192.168.12.14:1213");
  ck_assert_int_eq(ws_list_find(li, mt2), -1);
  ws_list_free(&li);
  ws_addr_free(&mt);
  ws_list_node_free(&ln);
}
END_TEST

START_TEST(test_wslist_exists)
{
  ws_list_t* li = ws_list_create();
  ws_list_node_t* ln = ws_list_node_create();
  ws_addr_t* mt = ws_addr_create();
  ws_addr_from_str(&mt, "192.168.12.14:1203");
  ln->data = mt;
  ws_list_add(li, ln);
  ck_assert_int_eq(ws_list_exists(li, mt), true);

  ws_addr_t* mt2 = ws_addr_create();
  ws_addr_from_str(&mt2, "192.168.12.14:1213");
  ck_assert_int_eq(ws_list_exists(li, mt2), false);
  ws_addr_free(&mt);

  ws_list_free(&li);
  ws_list_node_free(&ln);
}
END_TEST


Suite * wslist_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("WSlist");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_wslist_create);
    tcase_add_test(tc_core, test_wslist_free);
    tcase_add_test(tc_core, test_wslist_node_create);
    tcase_add_test(tc_core, test_wslist_node_create_values);
    tcase_add_test(tc_core, test_wslist_node_free);
    tcase_add_test(tc_core, test_wslist_length_0);
    tcase_add_test(tc_core, test_wslist_add);
    tcase_add_test(tc_core, test_wslist_length_2);
    tcase_add_test(tc_core, test_wslist_add_unique);
    tcase_add_test(tc_core, test_wslist_add_unique_addr);
    tcase_add_test(tc_core, test_wslist_add_unique_addr2);
    tcase_add_test(tc_core, test_wslist_add_unique_addr3);
    tcase_add_test(tc_core, test_wslist_find1);
    tcase_add_test(tc_core, test_wslist_find2);
    tcase_add_test(tc_core, test_wslist_exists);

    suite_add_tcase(s, tc_core);

    return s;
}
