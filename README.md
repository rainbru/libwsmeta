# libwsmeta

A ansi C websocket-based metaserver library designed to be used in multiple
rainbrurp* projects as git submodule.

## Dependencies

You'll need these dependencies to build `libwsmeta`:

	sudo apt install cmake libwebsockets-dev libcurl4-gnutls-dev libpcre3-dev \
	         check

On arch or manjaro :

	sudo pacman -Sy libwebsockets lcov

## Building

	mkdir ./build
	cd build
	cmake ..
	make
	make check

## Default port

The default port is defined in *CMakeLists.txt* as the **DEFAULT_PORT** 
variable.

```cmake
set(DEFAULT_PORT 16001)
```

## Using as a git submodule

You must generate configuration header in your *CMakeLists.txt* :

```cmake
configure_file(
  "${PROJECT_SOURCE_DIR}/<path to submodule>/wsmeta_config.h.in"
  "${PROJECT_BINARY_DIR}/wsmeta_config.h"
)
```

## Unit tests

If, for some reason, you suspect unit tests or you need to run them without
fork : 

	CK_FORK=no ./wsmeta-tests
